﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KeyperDev
{
	/// <summary>
	/// uMessageBox.xaml 的交互逻辑
	/// </summary>
	public partial class messagebox_getfingerprint : Window
	{
        private messagebox_getfingerprint()
        {
            InitializeComponent();
            m_SyncContext = SynchronizationContext.Current;
            lblMsg.Text = "指纹验证功能未开启\n请输入主密钥！";
            mp_suc.Open(new Uri(@"wav\successed.wav", UriKind.Relative));
            mp_fai.Open(new Uri(@"wav\failed.wav", UriKind.Relative));
            noUdun.Visibility = System.Windows.Visibility.Visible;
            Udun_insert.Visibility = System.Windows.Visibility.Hidden;
            s =(Storyboard) this.Resources["fingerLoadingStory"];
            //s.Begin();
        }
        private messagebox_getfingerprint(int regstOrCmp)
        {
            InitializeComponent();
            m_SyncContext = SynchronizationContext.Current;
            noUdun.Visibility = System.Windows.Visibility.Hidden;
            Udun_insert.Visibility = System.Windows.Visibility.Visible;
            fingerLoading.Visibility = System.Windows.Visibility.Hidden;
            mp_suc.Open(new Uri(@"wav\successed.wav", UriKind.Relative));
            mp_fai.Open(new Uri(@"wav\failed.wav", UriKind.Relative));
            if (regstOrCmp == 0) //注册指纹
            {
                this.regstOrCmp = 0;
                lblMsg_insert.Visibility = System.Windows.Visibility.Visible;
                lblMsg_regsorcmp.Visibility = System.Windows.Visibility.Hidden;
                tb_tishi_regsorcmp.Text = "";
                tb_tishi_regsorcmp.Visibility = System.Windows.Visibility.Hidden;
            }
            else   //验证指纹
            {
                this.regstOrCmp = 1;
                lblMsg_insert.Visibility = System.Windows.Visibility.Visible;
                lblMsg_regsorcmp.Visibility = System.Windows.Visibility.Hidden;
                tb_tishi_regsorcmp.Text = "";
                tb_tishi_regsorcmp.Visibility = System.Windows.Visibility.Hidden;
            }
            s =(Storyboard) this.Resources["fingerLoadingStory"];
            //s.Begin();
        }

        private int regstOrCmp = 0;
        public SynchronizationContext m_SyncContext = null;
        private user cu;
        private passwordRecord pwRcd;
        private string pwortishi;
        private string fingerprint;
        private securitystring securepwortishi = new securitystring();
        private int windowtype = 0;  //0add,1view,2edit,3del
        private Storyboard s = new Storyboard();
        private MediaPlayer mp_suc = new MediaPlayer();
        private MediaPlayer mp_fai = new MediaPlayer();
        /// <summary>
        /// 静态方法 模拟MESSAGEBOX.Show方法
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="msg">消息</param>
        /// <returns></returns>
        public static bool? Show(Window owner,user u, passwordRecord pwr, string pwots)
        {
            if (u.fp.useFP == 0)
            {
                var msgBox = new messagebox_getfingerprint();
                msgBox.cu = u;
                msgBox.pwRcd = pwr;
                msgBox.pwortishi = pwots;
                msgBox.Owner = owner;
                return msgBox.ShowDialog();
            }
            else 
            {
                var msgBox = new messagebox_getfingerprint(1);
                u.fp.iniSuccess = msgBox.iniSuccessCallback;
                u.fp.matchCaptured = msgBox.matchCapturedCallback;
                u.fp.regstCaptured = msgBox.regstCapturedCallback;
                msgBox.cu = u;
                msgBox.pwRcd = pwr;
                msgBox.pwortishi = pwots;
                msgBox.Owner = owner;
                u.fp.iniFpDevice();
                u.fp.cmpFinger();

                return msgBox.ShowDialog();
            }

        }

        public static bool? Show2(Window owner, user u, passwordRecord pwr, securitystring pwots)
        {
            if (u.fp.useFP == 0)
            {
                var msgBox = new messagebox_getfingerprint();
                msgBox.cu = u;
                msgBox.pwRcd = pwr;
                msgBox.securepwortishi = pwots;
                msgBox.windowtype = 1;
                msgBox.Owner = owner;
                return msgBox.ShowDialog();
            }
            else
            {
                var msgBox = new messagebox_getfingerprint(1);
                u.fp.iniSuccess = msgBox.iniSuccessCallback;
                u.fp.matchCaptured = msgBox.matchCapturedCallback;
                u.fp.regstCaptured = msgBox.regstCapturedCallback;
                msgBox.cu = u;
                msgBox.pwRcd = pwr;
                msgBox.securepwortishi = pwots;
                msgBox.windowtype = 1;
                msgBox.Owner = owner;
                u.fp.iniFpDevice();
                u.fp.cmpFinger();

                return msgBox.ShowDialog();
            }

        }
        public static bool? Show3(Window owner, user u)
        {
            if (u.fp.useFP == 0)
            {
                var msgBox = new messagebox_getfingerprint();
                msgBox.cu = u;
                msgBox.windowtype = 3;
                msgBox.Owner = owner;
                return msgBox.ShowDialog();
            }
            else
            {
                var msgBox = new messagebox_getfingerprint(1);
                u.fp.iniSuccess = msgBox.iniSuccessCallback;
                u.fp.matchCaptured = msgBox.matchCapturedCallback;
                u.fp.regstCaptured = msgBox.regstCapturedCallback;
                msgBox.cu = u;
                msgBox.windowtype = 3;
                msgBox.Owner = owner;
                u.fp.iniFpDevice();
                u.fp.cmpFinger();


                return msgBox.ShowDialog();
            }
           
        }
        public static bool? ShowRegst(Window owner, user u)
        {
            var msgBox = new messagebox_getfingerprint(0);
            u.fp.iniSuccess = msgBox.iniSuccessCallback;
            u.fp.matchCaptured = msgBox.matchCapturedCallback;
            u.fp.regstCaptured = msgBox.regstCapturedCallback;
            msgBox.cu = u;
            msgBox.windowtype = 3;
            msgBox.Owner = owner;
            u.fp.iniFpDevice();
            u.fp.regstFinger();
            return msgBox.ShowDialog();

        }
        public static bool? ShowClose(Window owner, user u)
        {
            var msgBox = new messagebox_getfingerprint(1);
            u.fp.iniSuccess = msgBox.iniSuccessCallback;
            u.fp.matchCaptured = msgBox.matchCapturedCallback;
            u.fp.regstCaptured = msgBox.regstCapturedCallback;
            msgBox.cu = u;
            msgBox.windowtype = 3;
            msgBox.Owner = owner;
            u.fp.iniFpDevice();
            u.fp.cmpFinger();
            return msgBox.ShowDialog();

        }
        public void iniSuccessCallback()
        {
            m_SyncContext.Post(iniSuccess,new object());
        }
        public void iniSuccess(object o) 
        {
            if (regstOrCmp == 0)
            {
                lblMsg_regsorcmp.Text = "请按指纹进行注册";
                lblMsg_insert.Visibility = System.Windows.Visibility.Hidden;
                lblMsg_regsorcmp.Visibility = System.Windows.Visibility.Visible;
                lblMsg_regsorcmp.Visibility = System.Windows.Visibility.Visible;
                tb_tishi_regsorcmp.Text = "";
                tb_tishi_regsorcmp.Visibility = System.Windows.Visibility.Visible;
                
            }
            else 
            {
                lblMsg_regsorcmp.Text = "请输入指纹";
                lblMsg_insert.Visibility = System.Windows.Visibility.Hidden;
                lblMsg_regsorcmp.Visibility = System.Windows.Visibility.Visible;
                lblMsg_regsorcmp.Visibility = System.Windows.Visibility.Visible;
                tb_tishi_regsorcmp.Text = "";
                tb_tishi_regsorcmp.Visibility = System.Windows.Visibility.Visible;
            }

            fingerLoading.Visibility = System.Windows.Visibility.Visible;
            s.Begin();
        }
        public void regstCapturedCallback(int i)
        {
            m_SyncContext.Post(regstCaptured,i);

        }
        public void regstCaptured(object o)
        {
            int i = (int)o;
            if (i == 4) 
            {
                tb_tishi_regsorcmp.Text = "指纹已被注册！";
                mp_fai.Position = new TimeSpan(0);
                mp_fai.Play();
            }
            else if (i == 3) 
            {
                tb_tishi_regsorcmp.Text = "请按同一指纹3次以注册！";
                mp_fai.Position = new TimeSpan(0);
                mp_fai.Play();
            }
            else if (i == 2) 
            {
                tb_tishi_regsorcmp.Text = "还需按2次该指纹！";
                mp_suc.Position = new TimeSpan(0);
                mp_suc.Play();
            }
            else if (i == 1)
            {
                tb_tishi_regsorcmp.Text = "还需按1次该指纹！";
                mp_suc.Position = new TimeSpan(0);
                mp_suc.Play();
            }
            else if (i == 0)
            {
                tb_tishi_regsorcmp.Text = "";
                
                cu.fp.stopDevice();
                mp_suc.Position = new TimeSpan(0);
                mp_suc.Play();
                uMessageBox.Show(this, "Keyper", "已成功注册指纹，将用该指纹验证您的身份为A级加密的账户提供保护！");



                this.DialogResult = true;
                this.Close();

            }
            else 
            {
                this.Visibility = System.Windows.Visibility.Hidden;
                cu.fp.stopregstFinger();
                cu.fp.stopDevice();
                uMessageBox.Show(this, "Keyper", "请重新插入U盾进行指纹的注册！");
                this.DialogResult = false;
                this.Close();
            }

        }
        public void matchCapturedCallback(int i)
        {
            m_SyncContext.Post(matchCaptured, i);

        }
        public void matchCaptured(object o)
        {
            int i = (int)o;
            if (i > 0)
            {
                cu.fp.stopDevice();
                tb_tishi_regsorcmp.Text = "";
                lblMsg.Text = "指纹验证通过\n请输入主密钥！";
                noUdun.Visibility = System.Windows.Visibility.Visible;
                Udun_insert.Visibility = System.Windows.Visibility.Hidden;
                mp_suc.Position = new TimeSpan(0);
                mp_suc.Play();
            }
            else 
            {
                tb_tishi_regsorcmp.Text = "指纹验证错误，请重新输入指纹！";
                mp_fai.Position = new TimeSpan(0);
                mp_fai.Play();
            
            }
        }

        private int wrongcode = 0;
        private void Yes_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (pwbox.Password.Length == 0) 
            {
                tb_tishi.Text = "请输入密码！";
                wrongcode = 1;
                return;
            }
            if (String.Compare(cryptogram.mySM3(pwbox.Password), cu.passwordHash) == 0)
            {
                if (windowtype == 0)
                {
                    pwbox.IsEnabled = false;
                    pwRcd.encryptedPassword = cryptogram.mySM4Encrpt(pwortishi, pwbox.Password);
                    pwbox.IsEnabled = true;
                }
                else if (windowtype == 1 )
                {
                    pwbox.IsEnabled = false;
                    securepwortishi.str = cryptogram.mySM4Decrpt(pwRcd.encryptedPassword, pwbox.Password);
                    pwbox.IsEnabled = true;
                }
                
                this.DialogResult = true;
                this.Close();
            }
            else 
            {
                tb_tishi.Text = "密码错误！";
                wrongcode = 1;
                return;
            }

            
        }


        private void No_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            cu.fp.stopini();
            if (cu.fp.useFP == 1) 
            {
                cu.fp.stopDevice();
            }
            this.DialogResult = false;
            this.Close();
        }


        private void inputnow(object sender, RoutedEventArgs e)
        {
            if (wrongcode == 1)
            {
                wrongcode = 0;
                tb_tishi.Text = "";
            }
        }

       
	}
}