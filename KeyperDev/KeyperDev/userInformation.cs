﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Xml;
using System.Security.Cryptography;
using System.IO;

namespace KeyperDev
{
    public class userInformation
    {
        #region 属性
        public user u = new user(); //用户信息文件
        public passwordRecords pwRecords = new passwordRecords();
        //保存所有的密码记录，程序每次运行每条记录都有唯一编号，方便管理（允许登录名相同，比如在不同的网站上有相同的登录名）
        public tempPasswordRecords tpra = new tempPasswordRecords();//临时存储
        public tempPasswordRecords tprb = new tempPasswordRecords();
        public tempPasswordRecords tprc = new tempPasswordRecords();
        private System.Text.Encoding encode = System.Text.Encoding.UTF8;
        byte[] data = new byte[1000];
        #endregion
        #region 方法       
        public int readUserInfoFromFile()   //从文件中读取账号信息（密码记录、临时密码记录）
        {
            u.fp = new Fingerprint(stringTObase(u.userName), stringTObase("finger"));
            if (Directory.Exists(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")))
            {///存在密码记录
                ///密码记录处理部分
                DirectoryInfo di = new DirectoryInfo(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord"));
                DirectoryInfo[] di2 = di.GetDirectories();
                foreach (DirectoryInfo d in di2)
                {
                    FileInfo[] inf = d.GetFiles();
                    foreach (FileInfo i in inf)
                    {
                        StreamReader srr = new StreamReader(i.FullName);//???
                        passwordRecord temp = new passwordRecord();
                        temp.passwordType = Convert.ToInt32(srr.ReadLine());
                        temp.passwordEncryptionLevel = Convert.ToInt32(srr.ReadLine());
                        temp.account = srr.ReadLine();
                        temp.user = srr.ReadLine();
                        temp.encryptedPassword = srr.ReadLine();
                        temp.description = srr.ReadLine();
                        temp.date = Convert.ToInt64(srr.ReadLine());
                        temp.pwortishi = Convert.ToInt32(srr.ReadLine());
                        int ii = pwRecords.maxid++;
                        pwRecords.passwordDic.Add(ii, temp);
                        srr.Close();
                    }
                }
            }
            if (Directory.Exists(stringTObase(u.userName) + @"\临时密码记录"))
            {///存在临时密码记录
                //临时密码记录处理部分
                if (Directory.Exists(stringTObase(u.userName) + @"\临时密码记录\" + "a"))
                {
                    DirectoryInfo tep = new DirectoryInfo(stringTObase(u.userName) + @"\临时密码记录\" + "a");
                    FileInfo[] inf = tep.GetFiles();
                    foreach (FileInfo i in inf)
                    {
                        StreamReader srr = new StreamReader(i.FullName);//???
                        tempPasswordRecord temp = new tempPasswordRecord();
                        temp.passwordType = Convert.ToInt32(srr.ReadLine());
                        temp.passwordEncryptionLevel = Convert.ToInt32(srr.ReadLine());
                        temp.account = srr.ReadLine();
                        temp.user = srr.ReadLine();
                        temp.encryptedPassword = srr.ReadLine();
                        temp.description = srr.ReadLine();
                        int ii = tpra.maxid++;
                        tpra.tempPasswordDic.Add(ii, temp);
                        srr.Close();
                    }
                }
                if (Directory.Exists(stringTObase(u.userName) + @"\临时密码记录\" + "b"))
                {
                    DirectoryInfo tep = new DirectoryInfo(stringTObase(u.userName) + @"\临时密码记录\" + "b");
                    FileInfo[] inf = tep.GetFiles();
                    foreach (FileInfo i in inf)
                    {
                        StreamReader srr = new StreamReader(i.FullName);//???
                        tempPasswordRecord temp = new tempPasswordRecord();
                        temp.passwordType = Convert.ToInt32(srr.ReadLine());
                        temp.passwordEncryptionLevel = Convert.ToInt32(srr.ReadLine());
                        temp.account = srr.ReadLine();
                        temp.user = srr.ReadLine();
                        temp.encryptedPassword = srr.ReadLine();
                        temp.description = srr.ReadLine();
                        int ii = tprb.maxid++;
                        tprb.tempPasswordDic.Add(ii, temp);
                        srr.Close();
                    }
                }
                if (Directory.Exists(stringTObase(u.userName) + @"\临时密码记录\" + "c"))
                {
                    DirectoryInfo tep = new DirectoryInfo(stringTObase(u.userName) + @"\临时密码记录\" + "c");
                    FileInfo[] inf = tep.GetFiles();
                    foreach (FileInfo i in inf)
                    {
                        StreamReader srr = new StreamReader(i.FullName);//???
                        tempPasswordRecord temp = new tempPasswordRecord();
                        temp.passwordType = Convert.ToInt32(srr.ReadLine());
                        temp.passwordEncryptionLevel = Convert.ToInt32(srr.ReadLine());
                        temp.account = srr.ReadLine();
                        temp.user = srr.ReadLine();
                        temp.encryptedPassword = srr.ReadLine();
                        temp.description = srr.ReadLine();
                        int ii = tprc.maxid++;
                        tprc.tempPasswordDic.Add(ii, temp);
                        srr.Close();
                    }
                }
            }
            return 0;
        }

        public int readUserInfoFromFile2()   //从文件中读取账号信息（密码记录、临时密码记录）
        {
           // u.fp = new Fingerprint(stringTObase(u.userName), stringTObase("finger"));
            u.fp.writefinger();
            if (Directory.Exists(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")))
            {///存在密码记录
                ///密码记录处理部分
                pwRecords.passwordDic.Clear();
                pwRecords.maxid = 0;
                DirectoryInfo di = new DirectoryInfo(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord"));
                DirectoryInfo[] di2 = di.GetDirectories();
                foreach (DirectoryInfo d in di2)
                {
                    FileInfo[] inf = d.GetFiles();
                    foreach (FileInfo i in inf)
                    {
                        StreamReader srr = new StreamReader(i.FullName);//???
                        passwordRecord temp = new passwordRecord();
                        temp.passwordType = Convert.ToInt32(srr.ReadLine());
                        temp.passwordEncryptionLevel = Convert.ToInt32(srr.ReadLine());
                        temp.account = srr.ReadLine();
                        temp.user = srr.ReadLine();
                        temp.encryptedPassword = srr.ReadLine();
                        temp.description = srr.ReadLine();
                        temp.date = Convert.ToInt64(srr.ReadLine());
                        temp.pwortishi = Convert.ToInt32(srr.ReadLine());
                        int ii = pwRecords.maxid++;
                        pwRecords.passwordDic.Add(ii, temp);
                        srr.Close();
                    }
                }
            }
            if (Directory.Exists(stringTObase(u.userName) + @"\临时密码记录"))
            {///存在临时密码记录
                //临时密码记录处理部分
                if (Directory.Exists(stringTObase(u.userName) + @"\临时密码记录\" + "a"))
                {
                    DirectoryInfo tep = new DirectoryInfo(stringTObase(u.userName) + @"\临时密码记录\" + "a");
                    FileInfo[] inf = tep.GetFiles();
                    foreach (FileInfo i in inf)
                    {
                        StreamReader srr = new StreamReader(i.FullName);//???
                        tempPasswordRecord temp = new tempPasswordRecord();
                        temp.passwordType = Convert.ToInt32(srr.ReadLine());
                        temp.passwordEncryptionLevel = Convert.ToInt32(srr.ReadLine());
                        temp.account = srr.ReadLine();
                        temp.user = srr.ReadLine();
                        temp.encryptedPassword = srr.ReadLine();
                        temp.description = srr.ReadLine();
                        int ii = tpra.maxid++;
                        tpra.tempPasswordDic.Add(ii, temp);
                        srr.Close();
                    }
                }
                if (Directory.Exists(stringTObase(u.userName) + @"\临时密码记录\" + "b"))
                {
                    DirectoryInfo tep = new DirectoryInfo(stringTObase(u.userName) + @"\临时密码记录\" + "b");
                    FileInfo[] inf = tep.GetFiles();
                    foreach (FileInfo i in inf)
                    {
                        StreamReader srr = new StreamReader(i.FullName);//???
                        tempPasswordRecord temp = new tempPasswordRecord();
                        temp.passwordType = Convert.ToInt32(srr.ReadLine());
                        temp.passwordEncryptionLevel = Convert.ToInt32(srr.ReadLine());
                        temp.account = srr.ReadLine();
                        temp.user = srr.ReadLine();
                        temp.encryptedPassword = srr.ReadLine();
                        temp.description = srr.ReadLine();
                        int ii = tprb.maxid++;
                        tprb.tempPasswordDic.Add(ii, temp);
                        srr.Close();
                    }
                }
                if (Directory.Exists(stringTObase(u.userName) + @"\临时密码记录\" + "c"))
                {
                    DirectoryInfo tep = new DirectoryInfo(stringTObase(u.userName) + @"\临时密码记录\" + "c");
                    FileInfo[] inf = tep.GetFiles();
                    foreach (FileInfo i in inf)
                    {
                        StreamReader srr = new StreamReader(i.FullName);//???
                        tempPasswordRecord temp = new tempPasswordRecord();
                        temp.passwordType = Convert.ToInt32(srr.ReadLine());
                        temp.passwordEncryptionLevel = Convert.ToInt32(srr.ReadLine());
                        temp.account = srr.ReadLine();
                        temp.user = srr.ReadLine();
                        temp.encryptedPassword = srr.ReadLine();
                        temp.description = srr.ReadLine();
                        int ii = tprc.maxid++;
                        tprc.tempPasswordDic.Add(ii, temp);
                        srr.Close();
                    }
                }
            }
            return 0;
        }
        public int addPasswordRecord(passwordRecord pr) //添加一个密码记录，并保存到文件   //返回值被pxs修改了= =
        {
            int i = pwRecords.maxid++;
            pwRecords.passwordDic.Add(i, pr);
            //然后保存到文件
            if (!Directory.Exists(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")))
            {//首先判断路径是否存在
                Directory.CreateDirectory(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord"));
            }
            if (!Directory.Exists(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account)))
            {
                Directory.CreateDirectory(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account));
            }
            if (File.Exists(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account) + @"\" + stringTObase(pr.user)))
            {
                return -1;//已经有相同的密码记录存在
            }
            File.Create(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account) + @"\" + stringTObase(pr.user)).Close();
            StreamWriter sw = new StreamWriter(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account) + @"\" + stringTObase(pr.user), false);
            sw.WriteLine(pr.passwordType.ToString());
            sw.WriteLine(pr.passwordEncryptionLevel.ToString());
            sw.WriteLine(pr.account);
            sw.WriteLine(pr.user);
            sw.WriteLine(pr.encryptedPassword);
            sw.WriteLine(pr.description);
            DateTime dt = DateTime.Now;
            pr.date = dt.Ticks;
            sw.WriteLine(pr.date);
            sw.WriteLine(pr.pwortishi);
            sw.Close();
            return i;
        }

        public int delPasswordRecord(int id) //删除一个密码记录，并保存到文件
        {
            passwordRecord pr = pwRecords.passwordDic[id];
            pwRecords.passwordDic.Remove(id);

            //然后保存到文件
            if (!Directory.Exists(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")))
            {//首先判断路径是否存在，不存在就有问题了
                return -1;
            }
            if (!Directory.Exists(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account)))
            {
                return -1;
            }
            if (!File.Exists(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account) + @"\" + stringTObase(pr.user)))
            {//如果不存在这个密码记录，还是有问题
                return -1;
            }
            File.Delete(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account) + @"\" + stringTObase(pr.user));
            return 0;
        }
        public int editPasswordRecord(int id,passwordRecord pr)//编辑一个密码记录，并保存到文件
        {
            passwordRecord tp = new passwordRecord(pwRecords.passwordDic[id]);
            pwRecords.passwordDic.Remove(id);
            pwRecords.passwordDic.Add(id, pr);
            //首先判断是不是改了account和user
            if ((tp.account != pr.account) || (tp.user != pr.user))
            {//修改了account或者user
                if (!File.Exists(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord") + @"\" + stringTObase(tp.account) + @"\" + stringTObase(tp.user)))
                {//怎么原有记录丢失了？
                    return -1;
                }
                File.Delete(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord") + @"\" + stringTObase(tp.account) + @"\" + stringTObase(tp.user));
                if (!Directory.Exists(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account)))
                {
                    Directory.CreateDirectory(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account));
                }
                if (File.Exists(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account) + @"\" + stringTObase(pr.user)))
                {
                    return -1;//已经有相同的密码记录存在
                }
                File.Create(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account) + @"\" + stringTObase(pr.user)).Close();
                StreamWriter ssw = new StreamWriter(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account) + @"\" + stringTObase(pr.user), false);
                ssw.WriteLine(pr.passwordType.ToString());
                ssw.WriteLine(pr.passwordEncryptionLevel.ToString());
                ssw.WriteLine(pr.account);
                ssw.WriteLine(pr.user);
                ssw.WriteLine(pr.encryptedPassword);
                ssw.WriteLine(pr.description);
                DateTime dt = DateTime.Now;
                pr.date = dt.Ticks;
                ssw.WriteLine(pr.date);
                ssw.WriteLine(pr.pwortishi);
                ssw.Close();
                return 0;//这里没有回复i 因为用不着吧
            }

            //然后保存到文件
            if (!Directory.Exists(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")))
            {//首先判断路径是否存在，不存在就有问题了
                return -1;
            }
            if (!Directory.Exists(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account)))
            {
                return -1;
            }
            if (!File.Exists(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account) + @"\" + stringTObase(pr.user)))
            {//如果不存在这个密码记录，还是有问题
                return -1;
            }
            StreamWriter sw = new StreamWriter(stringTObase(u.userName) + @"\" + stringTObase("SecretRecord")+@"\" + stringTObase(pr.account) + @"\" + stringTObase(pr.user), false);//false表示覆盖
            sw.WriteLine(pr.passwordType.ToString());
            sw.WriteLine(pr.passwordEncryptionLevel.ToString());
            sw.WriteLine(pr.account);
            sw.WriteLine(pr.user);
            sw.WriteLine(pr.encryptedPassword);
            sw.WriteLine(pr.description);
            DateTime dt2 = DateTime.Now;
            pr.date = dt2.ToFileTime();
            sw.WriteLine(pr.date);
            sw.WriteLine(pr.pwortishi);
            sw.Close();
            return 0;
         }

        public int addTempPasswordRecord(tempPasswordRecord tpr) //添加一个临时密码记录，暂不保存
        {
            int i;
            switch( tpr.passwordEncryptionLevel) 
            {
                case 0 :
                    i = tpra.maxid++;
                    tpra.tempPasswordDic.Add(i, tpr);
                    return 0;
                case 1 :
                    i = tprb.maxid++;
                    tprb.tempPasswordDic.Add(i, tpr);
                    return 0;
                case 2:
                    i = tprc.maxid++;
                    tprc.tempPasswordDic.Add(i, tpr);
                    return 0;
                default:
                    return -1;
            }

        }
        public int delTempPasswordRecord(int level, int id)
        {
            switch (level)
            {
                case 0:
                    tpra.tempPasswordDic.Remove(id);
                    return 0;
                case 1:
                    tprb.tempPasswordDic.Remove(id);
                    return 0;
                case 2:
                    tprc.tempPasswordDic.Remove(id);
                    return 0;
                default:
                    return -1;
            }
        }
        public int saveTempPasswordRecordToFile()//保存临时密码记录到文件中
        {
            if (!Directory.Exists(stringTObase(u.userName) + @"\临时密码记录"))
            {//首先判断路径是否存在
                Directory.CreateDirectory(stringTObase(u.userName) + @"\临时密码记录");
            }
            if (!Directory.Exists(stringTObase(u.userName) + @"\临时密码记录\" + "a"))
            {
                Directory.CreateDirectory(stringTObase(u.userName) + @"\临时密码记录\" + "a");
            }
            if (!Directory.Exists(stringTObase(u.userName) + @"\临时密码记录\" + "b"))
            {
                Directory.CreateDirectory(stringTObase(u.userName) + @"\临时密码记录\" + "b");
            }
            if (!Directory.Exists(stringTObase(u.userName) + @"\临时密码记录\" + "c"))
            {
                Directory.CreateDirectory(stringTObase(u.userName) + @"\临时密码记录\" + "c");
            }
            int maxnumm = 0;//用于临时存放密码记录的个数
            maxnumm = tpra.maxid;
            int i = 0;
            while (i++ < maxnumm)
            {
                if (!File.Exists(stringTObase(u.userName) + @"\临时密码记录\" + @"a\" + i.ToString()))
                {
                    File.Create(stringTObase(u.userName) + @"\临时密码记录\" + @"a\" + i.ToString()).Close();
                    StreamWriter sw = new StreamWriter(stringTObase(u.userName) + @"\临时密码记录\" + @"a\" + i.ToString());
                    sw.WriteLine(tpra.tempPasswordDic[i].passwordType.ToString());
                    sw.WriteLine(tpra.tempPasswordDic[i].passwordEncryptionLevel.ToString());
                    sw.WriteLine(tpra.tempPasswordDic[i].account);
                    sw.WriteLine(tpra.tempPasswordDic[i].user);
                    sw.WriteLine(tpra.tempPasswordDic[i].encryptedPassword);
                    sw.WriteLine(tpra.tempPasswordDic[i].description);
                    sw.Close();
                }
                else
                {//没有删除掉临时记录啊！
                    return -1;
                }
            }
            maxnumm = tprb.maxid;
            i = 0;
            while (i++ < maxnumm)
            {
                if (!File.Exists(stringTObase(u.userName) + @"\临时密码记录\" + @"b\" + i.ToString()))
                {
                    File.Create(stringTObase(u.userName) + @"\临时密码记录\" + @"b\" + i.ToString()).Close();
                    StreamWriter sw = new StreamWriter(stringTObase(u.userName) + @"\临时密码记录\" + @"b\" + i.ToString());
                    sw.WriteLine(tprb.tempPasswordDic[i].passwordType.ToString());
                    sw.WriteLine(tprb.tempPasswordDic[i].passwordEncryptionLevel.ToString());
                    sw.WriteLine(tprb.tempPasswordDic[i].account);
                    sw.WriteLine(tprb.tempPasswordDic[i].user);
                    sw.WriteLine(tprb.tempPasswordDic[i].encryptedPassword);
                    sw.WriteLine(tprb.tempPasswordDic[i].description);
                    sw.Close();
                }
                else
                {//没有删除掉临时记录啊！
                    return -1;
                }
            }
            maxnumm = tprc.maxid;
            i = 0;
            while (i++ < maxnumm)
            {
                if (!File.Exists(stringTObase(u.userName) + @"\临时密码记录\" + @"c\" + i.ToString()))
                {
                    File.Create(stringTObase(u.userName) + @"\临时密码记录\" + @"c\" + i.ToString()).Close();
                    StreamWriter sw = new StreamWriter(stringTObase(u.userName) + @"\临时密码记录\" + @"c\" + i.ToString());
                    sw.WriteLine(tprc.tempPasswordDic[i].passwordType.ToString());
                    sw.WriteLine(tprc.tempPasswordDic[i].passwordEncryptionLevel.ToString());
                    sw.WriteLine(tprc.tempPasswordDic[i].account);
                    sw.WriteLine(tprc.tempPasswordDic[i].user);
                    sw.WriteLine(tprc.tempPasswordDic[i].encryptedPassword);
                    sw.WriteLine(tprc.tempPasswordDic[i].description);
                    sw.Close();
                }
                else
                {//没有删除掉临时记录啊！
                    return -1;
                }
            }
            return 0;
        }
        private string baseTOstring(string st)
        {
            try
            {
                data = Convert.FromBase64String(st);
                return System.Text.ASCIIEncoding.Default.GetString(data);
            }
            catch
            {
                return st;
            }
            
        }
        public string stringTObase(string st)
        {
            try
            {
                data = encode.GetBytes(st);
            return Convert.ToBase64String(data, 0, data.Length);
            }
            catch
            {
                return st;
            }        
        }
        #endregion

    }

    public class passwordRecords
    {
        public int recordsNum = 0;
        public Dictionary<int, passwordRecord> passwordDic = new Dictionary<int, passwordRecord>();   //保存所有的密码记录，程序每次运行每条记录都有唯一编号，方便管理（允许登录名相同，比如在不同的网站上有相同的登录名）
        public int maxid = 0; //表示已经使用的最大编号
    }
    public class passwordRecord 
    {
        public int passwordType = 0;    //账号类型 0-社交；1-金融；2-工作；；3-学习;4-购物；5-娱乐
        public int passwordEncryptionLevel = 0;  //账号加密等级 0-A级；1-B级；2-C级     A级使用指纹密码加密，B级使用用户密码加密，C级直接使用程序加密
        public string account = "";     //账号名  QQ、微信等
        public string user = "";        //登录名
        public string encryptedPassword = "";    //加密后的密码
        public string description = "";          //描述
        public long date = 0; //当前操作日期
        public int pwortishi = 0; //0表示密码，1表示提示
        public passwordRecord()
        {

        }
        public passwordRecord(passwordRecord pr)
        {
            account = pr.account;
            passwordType = pr.passwordType;
            passwordEncryptionLevel = pr.passwordEncryptionLevel;
            user = pr.user;
            encryptedPassword = pr.encryptedPassword;
            description = pr.description;
            date = pr.date;
            pwortishi = pr.pwortishi;
        }
    }
    public class tempPasswordRecords
    {
        public int recordsNum = 0;
        public Dictionary<int, tempPasswordRecord> tempPasswordDic = new Dictionary<int, tempPasswordRecord>();
        public int maxid = 0;
    }
    public class tempPasswordRecord
    {
        public int passwordType = 0;    //账号类型 0-社交；1-金融；2-工作；3-购物；4-娱乐；5-学习
        public int passwordEncryptionLevel = 0;  //账号加密等级 0-A级；1-B级；2-C级     A级使用指纹密码加密，B级使用用户密码加密，C级直接使用程序加密
        public string account = "";     //账号名  QQ、微信等
        public string user = "";        //登录名
        public string encryptedPassword = "";    //加密后的密码(这里是指用程序加密，与用户无关，相当于C级)
        public string description = "";          //描述
        public tempPasswordRecord()
        {

        }
        public tempPasswordRecord(tempPasswordRecord pr)
        {
            account = pr.account;
            passwordType = pr.passwordType;
            passwordEncryptionLevel = pr.passwordEncryptionLevel;
            user = pr.user;
            encryptedPassword = pr.encryptedPassword;
            description = pr.description;
        }
    }
}

/**
1、base64  to  string

    string strPath =  "aHR0cDovLzIwMy44MS4yOS40Njo1NTU3L1
9iYWlkdS9yaW5ncy9taWRpLzIwMDA3MzgwLTE2Lm1pZA==";         
    byte[] bpath = Convert.FromBase64String(strPath);
    strPath = System.Text.ASCIIEncoding.Default.GetString(bpath);

2、string   to  base64
    System.Text.Encoding encode = System.Text.Encoding.ASCII ;
    byte[]  bytedata = encode.GetBytes( "test");
    string strPath = Convert.ToBase64String(bytedata,0,bytedata.Length);
*/