﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KeyperDev.UserControls
{
    /// <summary>
    /// pwlistElt.xaml 的交互逻辑
    /// </summary>
    public delegate void pwlisteltcallback(int addViewEditDel, int rcdid);    //实际应有参数   //addViewEditDel 0add 1view 2edit 3del   //rcdid 记录标识符，-1表示空记录
    public partial class pwlistElt : UserControl
    {
        public pwlistElt()
        {
            InitializeComponent();
            msinstory = (Storyboard)this.Resources["OnMouseEnter1"];
        }
        public Storyboard msinstory = new Storyboard();
        public pwlisteltcallback pwlisteltcallbackfunc;
        public int recordID = -1;        //记录标识符，-1表示空记录（即添加记录）
        private void msin(object sender, MouseEventArgs e)
        {
            msinstory.Begin();
        }

        private void mslv(object sender, MouseEventArgs e)
        {
            msinstory.Remove();
        }

        private void addnewpw(object sender, MouseButtonEventArgs e)
        {
            pwlisteltcallbackfunc(0,-1);
        }

        private void viewClicked(object sender, MouseButtonEventArgs e)
        {
            pwlisteltcallbackfunc(1, recordID);
        }

        private void editClicked(object sender, MouseButtonEventArgs e)
        {
            pwlisteltcallbackfunc(2, recordID);
        }

        private void delClicked(object sender, MouseButtonEventArgs e)
        {
            pwlisteltcallbackfunc(3, recordID);
        }

    }
}
