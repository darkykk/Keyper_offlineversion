﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KeyperDev.UserControls
{
    /// <summary>
    /// myButtton01.xaml 的交互逻辑
    /// </summary>
    public partial class myButtton01 : UserControl
    {
        public myButtton01()
        {
            InitializeComponent();
            sb = (Storyboard)this.Resources["OnMouseEnter1"];
        }
        private Storyboard sb;

        public static readonly DependencyProperty TextProperty =
           DependencyProperty.Register("Text", typeof(string),
           typeof(myButtton01),
           new PropertyMetadata("TextBlock", new PropertyChangedCallback(OnTextChanged)));
        public string Text
        {
            get { return (string)GetValue(TextProperty); }

            set { SetValue(TextProperty, value); }
        }

        static void OnTextChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            myButtton01 source = (myButtton01)sender;
            source.btb.Text = (string)args.NewValue;
            if (source.btb.Text.Length < 3)
            {
                source.button_bg.Width = 77;
                source.btb.Width = 65;
            }
            else
            {
                source.button_bg.Width = 103;
                source.btb.Width = 90;
            }
        }


        public static readonly DependencyProperty blueOrRedProperty =
          DependencyProperty.Register("blueOrRed", typeof(bool),
          typeof(myButtton01),
          new PropertyMetadata(true, new PropertyChangedCallback(OnblueOrRedChanged)));
       
        public bool blueOrRed {

            get { return (bool)GetValue(blueOrRedProperty); }
            set { SetValue(blueOrRedProperty, value); }
        }

        static void OnblueOrRedChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            myButtton01 source = (myButtton01)sender;
            source.blueOrRed = (bool)args.NewValue;
            if (source.blueOrRed == true)
            {
                source.button_bg.Fill = source.path_blue.Fill;
                source.button_bg.Stroke = source.path_blue1.Fill;
            }
            else
            {

                source.button_bg.Fill = source.path_red.Fill;
                source.button_bg.Stroke = source.path_red1.Fill;
            }
        }



        #region msin
        public static readonly RoutedEvent MyButtonMsinEvent =
                    EventManager.RegisterRoutedEvent("MyButtonMsinEvent", RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<object>), typeof(myButtton01));

        public event RoutedPropertyChangedEventHandler<object> MyButtonMsin
        {
            add
            {
                this.AddHandler(MyButtonMsinEvent, value);
            }

            remove
            {
                this.RemoveHandler(MyButtonMsinEvent, value);
            }
        }



        #endregion
        public static readonly RoutedEvent MyButtonMsbdEvent =
                    EventManager.RegisterRoutedEvent("MyButtonMsbdEvent", RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<object>), typeof(myButtton01));

        public event RoutedPropertyChangedEventHandler<object> MyButtonMsbd
        {
            add
            {
                this.AddHandler(MyButtonMsbdEvent, value);
            }

            remove
            {
                this.RemoveHandler(MyButtonMsbdEvent, value);
            }
        }




        public static readonly RoutedEvent MyButtonMslvEvent =
                   EventManager.RegisterRoutedEvent("MyButtonMslvEvent", RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<object>), typeof(myButtton01));

        public event RoutedPropertyChangedEventHandler<object> MyButtonMslv
        {
            add
            {
                this.AddHandler(MyButtonMslvEvent, value);
            }

            remove
            {
                this.RemoveHandler(MyButtonMslvEvent, value);
            }
        }


        public static readonly RoutedEvent MyButtonMslbuEvent =
                  EventManager.RegisterRoutedEvent("MyButtonMslbuEvent", RoutingStrategy.Bubble, typeof(RoutedPropertyChangedEventHandler<object>), typeof(myButtton01));

        public event RoutedPropertyChangedEventHandler<object> MyButtonMslbu
        {
            add
            {
                this.AddHandler(MyButtonMslbuEvent, value);
            }

            remove
            {
                this.RemoveHandler(MyButtonMslbuEvent, value);
            }
        }



        private void Msin(object sender, MouseEventArgs e)
        {
            RoutedPropertyChangedEventArgs<object> arg =
               new RoutedPropertyChangedEventArgs<object>(sender, e, MyButtonMsinEvent);

            this.RaiseEvent(arg);
            button_bg.Opacity = 1;
        }

        private void Mslbu(object sender, MouseButtonEventArgs e)
        {
            RoutedPropertyChangedEventArgs<object> arg =
         new RoutedPropertyChangedEventArgs<object>(sender, e, MyButtonMslbuEvent);

            this.RaiseEvent(arg);
            button_bg.Opacity = 1;

        }

        private void Mslv(object sender, MouseEventArgs e)
        {
            RoutedPropertyChangedEventArgs<object> arg =
               new RoutedPropertyChangedEventArgs<object>(sender, e, MyButtonMslvEvent);

            this.RaiseEvent(arg);
            button_bg.Opacity = 0.47;
        }

        private void Msbd(object sender, MouseButtonEventArgs e)
        {
            RoutedPropertyChangedEventArgs<object> arg =
                new RoutedPropertyChangedEventArgs<object>(sender, e, MyButtonMsbdEvent);

            this.RaiseEvent(arg);
            button_bg.Opacity = 0.47;
        }


      
    }
}
