﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KeyperDev.UserControls
{
    /// <summary>
    /// pwlistElt.xaml 的交互逻辑
    /// </summary>
    public delegate void msglisteltcallback(int addViewEditDel, int rcdid);    //实际应有参数   //addViewEditDel 0add 1view 2edit 3del   //rcdid 记录标识符，-1表示空记录
    public partial class msglistElt : UserControl
    {
        public msglistElt()
        {
            InitializeComponent();
        }

        private void mouseenter(object sender, MouseEventArgs e)
        {
            recbc0.Opacity = 0.6;
            recbc2.Opacity = 1;
        }

        private void mouseleave(object sender, MouseEventArgs e)
        {
            recbc0.Opacity = 0;
            recbc2.Opacity = 0.7;
        }
    }
}
