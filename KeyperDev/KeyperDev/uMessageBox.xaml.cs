﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KeyperDev
{
	/// <summary>
	/// uMessageBox.xaml 的交互逻辑
	/// </summary>
	public partial class uMessageBox : Window
	{
        public static SynchronizationContext m_SyncContext = null;
		private uMessageBox()
        {
            InitializeComponent();
            m_SyncContext = SynchronizationContext.Current;
        }
        public generatePwMessage gpm = new generatePwMessage();
        public new string Title
        {
            get { return this.lblTitle.Text; }
            set { this.lblTitle.Text = value; }
        }

        public string Message_ext
        {
            get { return this.lblMsg_ext.Text; }
            set { this.lblMsg_ext.Text = value; }
        }
        public string Title_ext
        {
            get { return this.lblTitle_ext.Text; }
            set { this.lblTitle_ext.Text = value; }
        }

        public string Message
        {
            get { return this.lblMsg.Text; }
            set { this.lblMsg.Text = value; }
        }

        private BackUpinfo bui = new BackUpinfo();
   
        /// <summary>
        /// 静态方法 模拟MESSAGEBOX.Show方法
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="msg">消息</param>
        /// <returns></returns>
        public static bool? Show(Window owner,string title,string msg)
        {
            var msgBox = new uMessageBox();
            msgBox.Title = title;
            msgBox.Message = msg;
            msgBox.Owner = owner;
            return msgBox.ShowDialog();
        }
        public static bool? Show2(Window owner,string title, string msg, string buttonFalseText, string buttonTrueText)
        {
            var msgBox = new uMessageBox();
            msgBox.Title = title;
            msgBox.Message = msg;
            msgBox.Owner = owner;
            msgBox.falseTextblock.Text = buttonFalseText;
            msgBox.trueTextblock.Text = buttonTrueText;
            msgBox.border1.Visibility = Visibility.Hidden;
            msgBox.border2.Visibility = Visibility.Visible;
            msgBox.border3.Visibility = Visibility.Visible;
            return msgBox.ShowDialog();
        }
        public static bool? Show2(Window owner, string title, string msg)
        {
            var msgBox = new uMessageBox();
            msgBox.Title = title;
            msgBox.BuRecMsg.Text = msg;
            msgBox.Owner = owner;

            msgBox.border1.Visibility = Visibility.Hidden;
            msgBox.border2.Visibility = Visibility.Visible;
            msgBox.border3.Visibility = Visibility.Visible;
            return msgBox.ShowDialog();
        }
        public static bool? Show3(Window owner, string title, string msg,generatePwMessage gpm)
        {
            
            var msgBox = new uMessageBox();
            msgBox.Title_ext = title;
            msgBox.Message_ext = msg;
            msgBox.Owner = owner;
            msgBox.gpm = gpm;
            msgBox.originCvs.Visibility = Visibility.Hidden;
            msgBox.extendCvs.Visibility = Visibility.Visible;
            
            return msgBox.ShowDialog();
        }

        private BackUp bu;

        public static bool? ShowBackup(Window owner,BackUpinfo BUI)
        {
            var msgBox = new uMessageBox();
            msgBox.Title = "Keyper";
            msgBox.Owner = owner;
            msgBox.bui = BUI;
            msgBox.showBuRecMsg(0);
            ///here

            return msgBox.ShowDialog();
        }
        public  void showBuRecMsg(int state)
        {
            if (state == 0)
            {
                StringBuilder strbuilder = new StringBuilder();
                strbuilder.Clear();
                if (bui.LocalOrOnline == 0)
                {
                    strbuilder.Append("是否进行本地");
                }
                else
                {
                    strbuilder.Append("是否进行云端");
                }

                if (bui.BuOrRec == 0)   //backup
                {
                    strbuilder.Append("备份？\n");

                }
                else
                {
                    strbuilder.Append("恢复？\n");
                }


                if (bui.LocalOrOnline == 0 && bui.BuOrRec == 0)
                {
                    strbuilder.Append("备份用户：" + bui.username + "\n");
                    strbuilder.Append("备份位置：" + bui.filename + "\n");
                }
                else if (bui.LocalOrOnline == 0 && bui.BuOrRec == 1)
                {
                    strbuilder.Append("恢复位置：" + bui.filename + "\n");
                }
                else if (bui.LocalOrOnline == 1 && bui.BuOrRec == 0)
                {
                    strbuilder.Append("备份用户：" + bui.username + "\n");
                }
                else
                {
                    strbuilder.Append("恢复码：" + bui.backupCode + "\n");
                }
                if (strbuilder.Length > 45) 
                {
                    strbuilder.Remove(45, strbuilder.Length - 45);
                    strbuilder.Append("...");
                }
                lblMsg_BR.Text = strbuilder.ToString();
                border4.Visibility = Visibility.Hidden;
                border5.Visibility = Visibility.Visible;
                border6.Visibility = Visibility.Visible;
                BuRecCvs.Visibility = Visibility.Hidden;
                originCvs.Visibility = Visibility.Hidden;
                originCvs_BuRec.Visibility = Visibility.Visible;
                extendCvs.Visibility = Visibility.Hidden;

            }
            else 
            {
            
            
            }
        
        
        }
        private void burcYes_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            bu = new BackUp(bui.ul);
            if (bui.LocalOrOnline == 0 && bui.BuOrRec == 0)
            {
                BuRecMsg.Text="开始本地备份";
                (new Thread(backupThreadFunc)).Start();
            }
            else if (bui.LocalOrOnline == 0 && bui.BuOrRec == 1)
            {
                BuRecMsg.Text = "开始本地恢复";
                (new Thread(recoveryThreadFunc)).Start();
                
            }
            else if (bui.LocalOrOnline == 1 && bui.BuOrRec == 0)
            {
                BuRecMsg.Text = "开始云备份";
                
            }
            else
            {
                BuRecMsg.Text = "开始云恢复";
            }

            BuRecCvs.Visibility = Visibility.Visible;
            originCvs.Visibility = Visibility.Hidden;
            originCvs_BuRec.Visibility = Visibility.Hidden;
            extendCvs.Visibility = Visibility.Hidden;
        }


        private void backupThreadFunc() 
        {
           
            if (bu.setBackupUser(bui.username) == 0)
            {
                m_SyncContext.Post(solveBackup,0);
                if (bu.DoBackup(bui.filename) != null)
                {
                    Thread.Sleep(3000);
                    m_SyncContext.Post(solveBackup, 1);
                }
                else 
                {
                    m_SyncContext.Post(solveBackup, -2);
                }
            }
            else 
            {
                m_SyncContext.Post(solveBackup,-1); //用户不存在
            }
          
        }
        private void recoveryThreadFunc() 
        {

            m_SyncContext.Post(solveRecovery, 0);
            if (bu.DoRecovery(bui.filename) == 0)
            {
                Thread.Sleep(3000);
                
                m_SyncContext.Post(solveRecovery, 1);
            }
            else
            {
                m_SyncContext.Post(solveRecovery, -1);
                solveRecovery(-1);
            
            }
        
        }
        private bool recbackupResult = true; 
        private Storyboard loadingstoryboard = new Storyboard();
        private void solveRecovery(object o) 
        {
            int state = (int)o;
            if (state == 0)
            {
                BuRecMsg.Text = "正在恢复数据";
                loadingstoryboard = (Storyboard)this.Resources["loadingStoryBoard"];
                loadingstoryboard.Begin();

            }
            else if (state == 1)
            {
                lblTitle1.Text = "Keyper";
                lblMsg_BR.Text = "恢复成功！点击确定返回。";
                border4.Visibility = System.Windows.Visibility.Visible;
                border5.Visibility = System.Windows.Visibility.Hidden;
                border6.Visibility = System.Windows.Visibility.Hidden;
                originCvs.Visibility = System.Windows.Visibility.Hidden;
                extendCvs.Visibility = System.Windows.Visibility.Hidden;
                originCvs_BuRec.Visibility = System.Windows.Visibility.Visible;
                loadingstoryboard.Remove();
                BuRecCvs.Visibility = System.Windows.Visibility.Hidden;
            }
            else if (state == -1)
            {
                lblTitle1.Text = "Keyper";
                lblMsg_BR.Text = "十分抱歉，恢复失败！所选文件不是有效的备份文件。";
                recbackupResult = false;
                border4.Visibility = System.Windows.Visibility.Visible;
                border5.Visibility = System.Windows.Visibility.Hidden;
                border6.Visibility = System.Windows.Visibility.Hidden;
                originCvs.Visibility = System.Windows.Visibility.Hidden;
                extendCvs.Visibility = System.Windows.Visibility.Hidden;
                originCvs_BuRec.Visibility = System.Windows.Visibility.Visible;
                loadingstoryboard.Remove();
                BuRecCvs.Visibility = System.Windows.Visibility.Hidden;

            }
        
        }
        private void solveBackup(object o) 
        {
            int state = (int)o;
            if (state == 0) 
            {
                BuRecMsg.Text = "正在备份数据";
                loadingstoryboard = (Storyboard)this.Resources["loadingStoryBoard"];
                loadingstoryboard.Begin();

            }
            else if (state == 1) 
            {
                lblTitle1.Text = "Keyper";
                lblMsg_BR.Text = "备份成功！点击确定返回。";
                border4.Visibility = System.Windows.Visibility.Visible;
                border5.Visibility = System.Windows.Visibility.Hidden;
                border6.Visibility = System.Windows.Visibility.Hidden;
                originCvs.Visibility = System.Windows.Visibility.Hidden;
                extendCvs.Visibility = System.Windows.Visibility.Hidden;
                originCvs_BuRec.Visibility = System.Windows.Visibility.Visible;
                loadingstoryboard.Remove();
                BuRecCvs.Visibility = System.Windows.Visibility.Hidden;
            }
            else if (state == -1  || state == -2) 
            {
                lblTitle1.Text = "Keyper";
                lblMsg_BR.Text = "十分抱歉，备份失败！请尝试重启程序后再试QAQ";
                recbackupResult = false;
                border4.Visibility = System.Windows.Visibility.Visible;
                border5.Visibility = System.Windows.Visibility.Hidden;
                border6.Visibility = System.Windows.Visibility.Hidden;
                originCvs.Visibility = System.Windows.Visibility.Hidden;
                extendCvs.Visibility = System.Windows.Visibility.Hidden;
                originCvs_BuRec.Visibility = System.Windows.Visibility.Visible;
                loadingstoryboard.Remove();
                BuRecCvs.Visibility = System.Windows.Visibility.Hidden;
            
            }
          
        }
        


        
        private void Yes_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (recbackupResult == false) 
            {
                this.DialogResult = false;
                this.Close();
                return;
            }
            this.DialogResult = true;
            this.Close();
        }


        private void No_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void ext_Yes_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (numtb.Text.Length == 0)
            {
                tb_pwlehgthtishi.Text = "请输入长度！";
                return;
            }
            else 
            {
                try
                {
                    int i = Convert.ToInt32(numtb.Text);
                    if (i > 5 && i < 20)
                    {
                        gpm.l = i;
                        gpm.containflag = rb_contain.IsChecked;
                        this.DialogResult = true;
                        this.Close();
                        return;
                    }
                    else
                    {
                        if (i < 6)
                        {
                            tb_pwlehgthtishi.Text = "长度不应少于6！";
                        }
                        else
                        {
                            tb_pwlehgthtishi.Text = "长度不应多于20！";
                        }
                        return;
                    }

                }
                catch (Exception ec) 
                {
                    tb_pwlehgthtishi.Text = "请输入数字！";
                    numtb.Clear();
                    return;
                }
            }
        }

        


	}

    public class generatePwMessage 
    {
        public int _l;
        public int l
        {
            get {
                return this._l; }
            set {
                if (value < 6)
                    this._l = 6;
                else if (value > 20)
                    this._l = 20;
                else
                    this._l = value;
                }
        }

        public bool? containflag = false;
    }
}