﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.IO;

namespace KeyperDev
{
    public class userList
    {
        #region 属性
        public int userNum = 0;
        public ArrayList userArrayList = new ArrayList();

        #endregion
        #region 方法
        public int writeUsersFromFile(user u)//将用户列表写入文件中
        {
            if (!File.Exists("用户列表"))
            {
                File.Create("用户列表").Close();
            }
            StreamWriter sw = new StreamWriter("用户列表", true);
            sw.WriteLine(u.userName);
            sw.WriteLine(u.passwordHash);
            sw.WriteLine(u.fingerprintCodeHash);
            //sw.WriteLine(u.userIcon.ToString());
            sw.Close();
            return 0;
        }
        public  int loadUsersFromFile()  //未实现 从文件读取用户列表 0表示读取成功
        {
            int i = 1;
            if (!File.Exists("用户列表"))
            {
                return -1;
            }
            user u = new user();
            StreamReader sr = new StreamReader("用户列表");
            if ((u.userName = sr.ReadLine()) == null)
            {
                return 0;
            }
            u.passwordHash = sr.ReadLine();
            u.fingerprintCodeHash = sr.ReadLine();
            userArrayList.Add(new user(u));

            for (i = 1; (u.userName = sr.ReadLine()) != null; i++)
            {
                if (i > 300)
                {
                    return -1;
                }
                u.passwordHash = sr.ReadLine();
                u.fingerprintCodeHash = sr.ReadLine();
                //u.userIcon.Source = new BitmapImage(new Uri(@"img/touxiang.jpg", UriKind.Relative));
                userArrayList.Add(new user(u));
            }
            userNum = userArrayList.Count;
            sr.Close();
            return i;
        }
        #endregion
    }
    public class user
    {
        public string userName = "";        //用户名
        public string passwordHash = "";    //密码HASH
        public string fingerprintCodeHash = "";   //指纹密码HASH
        //public Image userIcon = new Image();      //用户头像
        public Fingerprint fp;
        public user()
        {

        }
        public user(string userName2, string passwordHash2, string fingerprintCodeHash2)
        {
            userName = userName2;
            passwordHash = passwordHash2;
            fingerprintCodeHash = fingerprintCodeHash2;
        }
        public user(user USE)
        {
            this.userName = USE.userName;
            this.passwordHash = USE.passwordHash;
            this.fingerprintCodeHash = USE.fingerprintCodeHash;
        }
    }
}
