﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace KeyperDev
{
	/// <summary>
	/// uMessageBox.xaml 的交互逻辑
	/// </summary>
	public partial class messagebox_getUserPw : Window
	{
        private messagebox_getUserPw()
        {
            InitializeComponent();
        }


        private user cu;
        private passwordRecord pwRcd;
        private string pwortishi;
        private securitystring securepwortishi = new securitystring();
        private int windowtype = 0;  //0add,1view,2edit,3del
        /// <summary>
        /// 静态方法 模拟MESSAGEBOX.Show方法
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="msg">消息</param>
        /// <returns></returns>
        public static bool? Show(Window owner,user u, passwordRecord pwr, string pwots)
        {
            var msgBox = new messagebox_getUserPw();
            msgBox.cu = u;
            msgBox.pwRcd = pwr;
            msgBox.pwortishi = pwots;
            msgBox.Owner = owner;
            return msgBox.ShowDialog();
        }
        public static bool? Show2(Window owner, user u, passwordRecord pwr, securitystring pwots)
        {
            var msgBox = new messagebox_getUserPw();
            msgBox.cu = u;
            msgBox.pwRcd = pwr;
            msgBox.securepwortishi = pwots;
            msgBox.windowtype = 1;
            msgBox.Owner = owner;
            return msgBox.ShowDialog();
        }
        public static bool? Show3(Window owner, user u)
        {
            var msgBox = new messagebox_getUserPw();
            msgBox.cu = u;
            msgBox.windowtype = 3;
            msgBox.Owner = owner;
            return msgBox.ShowDialog();
        }
        private int wrongcode = 0;
        private void Yes_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (pwbox.Password.Length == 0) 
            {
                tb_tishi.Text = "请输入密码！";
                wrongcode = 1;
                return;
            }
            if (String.Compare(cryptogram.mySM3(pwbox.Password), cu.passwordHash) == 0)
            {
                if (windowtype == 0)
                {
                    pwbox.IsEnabled = false;
                    pwRcd.encryptedPassword = cryptogram.mySM4Encrpt(pwortishi, pwbox.Password);
                    pwbox.IsEnabled = true;
                }
                else if (windowtype == 1)
                {
                    pwbox.IsEnabled = false;
                    securepwortishi.str = cryptogram.mySM4Decrpt(pwRcd.encryptedPassword, pwbox.Password);
                    pwbox.IsEnabled = true;
                }

                this.DialogResult = true;
                this.Close();
            }
            else 
            {
                tb_tishi.Text = "密码错误！";
                wrongcode = 1;
                return;
            }

            
        }


        private void No_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }


        private void inputnow(object sender, RoutedEventArgs e)
        {
            if (wrongcode == 1)
            {
                wrongcode = 0;
                tb_tishi.Text = "";
            }
        }


	}
}